package com.android.learn.urbaneateries.enumdef;

/**
 * Created by jame_h on 10/5/2016.
 */
/*public enum EnumOrderStatus {
    ORDER_IN_CART,                       // Order still in shopping cart, not checked out
    ORDER_SENT_FOR_CONFIRMATION_BY_CHEF, // Order send to chef for confirmation before checking out
    ORDER_CANCELLED_BY_CUSTOMER,         // Order cacncelled by customer
    ORDER_CONFIRMED_BY_CHEF,             // Order has been confirmed by chef
    ORDER_REJECTED_BY_CHEF,              // Order rejected by chef
    ORDER_CHECKED_OUT_BY_CUSTOMER,       // Order has been checked out by customer
    ORDER_IN_PREPARATION_BY_CHEF,        // Order prepartion in progress by chef ( updated by chef )
    ORDER_COMPLETE_BY_CHEF,              // Order completed by chef ( updated by chef )
    ORDER_READY_FOR_PICK_UP,             // Order ready for pick up by customer at designated location
    ORDER_READY_FOR_DELIVERY,            // Order ready for delivery to customer by chef or chef associates
    ORDER_DELIVERED_TO_CUSTOMER,         // Order delivered to customer
}*/


public enum EnumOrderStatus
{
    ORDER_IN_CART,
    ORDER_PENDING_CONFIRMATION,
    ORDER_ACCEPTED,
    ORDER_REJECTED,
    ORDER_ACTIVE,
    ORDER_CLOSED,
}
