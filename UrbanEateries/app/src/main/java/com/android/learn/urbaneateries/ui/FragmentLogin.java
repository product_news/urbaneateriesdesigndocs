package com.android.learn.urbaneateries.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.learn.urbaneateries.HttpApiClient;
import com.android.learn.urbaneateries.R;
import com.android.learn.urbaneateries.model.LoginInfo;
import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by jame_h on 10/6/2016.
 */
public class FragmentLogin extends android.support.v4.app.Fragment
        implements HttpApiClient.OnVerifyLoginListener
{
    private static final String TAG = "FragmentLogin";
    Button mButtonLogin;
    Button mButtonCancel;
    private EditText mEditTextEmail;
    private EditText mEditTextPassword;
    private TextView mTextViewLoginFailed;
    private TextView mTextViewForgotPassword;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState )
    {
        super.onCreateView( inflater, container, savedInstanceState );

        View rootView = (View) inflater.inflate( R.layout.fragment_login, container, false );

        mButtonLogin = (Button) rootView.findViewById( R.id.fragment_login_button_login );
        mButtonCancel = (Button) rootView.findViewById( R.id.fragment_login_button_cancel );
        mEditTextEmail = (EditText) rootView.findViewById( R.id.fragment_login_edit_text_login_email_add );
        mEditTextPassword = (EditText) rootView.findViewById( R.id.fragment_login_edit_text_login_password );
        mTextViewLoginFailed = (TextView) rootView.findViewById( R.id.fragment_login_text_view_login_failed );
        mTextViewForgotPassword = (TextView) rootView.findViewById( R.id.fragment_login_forgot_password );

        return rootView;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
    {
        super.onViewCreated( view, savedInstanceState );


        mButtonLogin.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mTextViewLoginFailed.setVisibility( View.INVISIBLE );

                HttpApiClient httpApiClient = HttpApiClient.getSharedIntance( getActivity().getApplicationContext() );
                LoginInfo loginInfo = new LoginInfo( mEditTextEmail.getText().toString(), mEditTextPassword.getText().toString() );
                JSONObject jsonObject = loginInfo.toJson();

                httpApiClient.verifyLoginUser( jsonObject, FragmentLogin.this );
            }
        } );

        mButtonCancel.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                getActivity().setResult( Activity.RESULT_CANCELED );
                getActivity().finish();
            }
        } );

        mTextViewForgotPassword.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace( R.id.activity_signup_login_container, new FragmentForgotPassword() )
                        .addToBackStack( TAG )
                        .commit();
            }
        } );
    }

    @Override
    public void onVerifyLoginComplete( JSONObject response, VolleyError error )
    {
        if( response != null )
        {
            getActivity().setResult( Activity.RESULT_OK );
            getActivity().finish();
        }
        else
        {
            mTextViewLoginFailed.setVisibility( View.VISIBLE );
        }
    }
}
