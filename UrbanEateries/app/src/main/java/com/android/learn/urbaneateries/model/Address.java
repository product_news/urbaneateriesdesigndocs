package com.android.learn.urbaneateries.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by jame_h on 10/5/2016.
 */
public class Address implements Parcelable
{
    public static final Creator<Address> CREATOR = new Creator<Address>()
    {
        @Override
        public Address createFromParcel( Parcel in )
        {
            return new Address( in );
        }

        @Override
        public Address[] newArray( int size )
        {
            return new Address[size];
        }
    };
    String mID;
    String mLabel;
    String mStreet;
    String mCity;
    int mZipCode;
    String mState;
    String mCountry;

    public Address( JSONObject jsonObject )
    {
        try
        {
            mID = jsonObject.getString( "objectId" );
            mLabel = jsonObject.getString( "Label" );
            mState = jsonObject.getString( "Street" );
            mCity = jsonObject.getString( "City" );
            mZipCode = jsonObject.getInt( "ZipCode" );
            mState = jsonObject.getString( "State" );
            mCountry = jsonObject.getString( "Country" );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }
    }

    protected Address( Parcel in )
    {
        mID = in.readString();
        mLabel = in.readString();
        mStreet = in.readString();
        mCity = in.readString();
        mZipCode = in.readInt();
        mState = in.readString();
        mCountry = in.readString();
    }

    public JSONObject toJson()
    {
        JSONObject jsonObject = new JSONObject();

        try
        {
            jsonObject.put( "___class", "Address" );
            jsonObject.put( "Label", this.mLabel );
            jsonObject.put( "Street", this.mStreet );
            jsonObject.put( "City", this.mCity );
            jsonObject.put( "ZipCode", this.mZipCode );
            jsonObject.put( "State", this.mState );
            jsonObject.put( "Country", this.mCountry );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override public int describeContents()
    {
        return 0;
    }

    @Override public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( mID );
        dest.writeString( mLabel );
        dest.writeString( mStreet );
        dest.writeString( mCity );
        dest.writeInt( mZipCode );
        dest.writeString( mState );
        dest.writeString( mCountry );
    }
}
