package com.android.learn.urbaneateries.model;

import com.android.learn.urbaneateries.enumdef.EnumOrderStatus;

import java.util.Date;
import java.util.List;

/**
 * Created by jame_h on 10/5/2016.
 */
public class Order
{
    String mID;
    List< OrderItem > mOrderItemList;
    Date mOrderDate;
    EnumOrderStatus mOrderStatus;
}
