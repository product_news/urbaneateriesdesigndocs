package com.android.learn.urbaneateries.ui.ChefMenu;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.learn.urbaneateries.R;
import com.android.learn.urbaneateries.model.ChefAccount;
import com.android.learn.urbaneateries.model.MenuItem;
import com.google.android.gms.vision.text.Text;

/**
 * Created by Jame on 10/22/2016.
 */

public class ChefMenuItemViewHolder extends RecyclerView.ViewHolder
{
    TextView mMenuItemTitleTextView;
    TextView mMenuItemPriceTextView;
    OnMenuItemSelectedListener mMenuItemSelectedListener;
    MenuItem mMenuItem;

    public ChefMenuItemViewHolder( View menuItemView, OnMenuItemSelectedListener handler )
    {
        super( menuItemView );
        mMenuItemTitleTextView = (TextView) menuItemView.findViewById( R.id.fragment_chef_menu_item_title_text_view );
        mMenuItemPriceTextView = (TextView) menuItemView.findViewById( R.id.fragment_chef_menu_item_price_text_view );
        mMenuItemSelectedListener = handler;

        menuItemView.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mMenuItemSelectedListener.onMenuItemSelected( mMenuItem );
            }
        } );
    }

    public void setMenuItemDetails( MenuItem menuItem )
    {
        mMenuItem = menuItem;
        mMenuItemTitleTextView.setText( menuItem.getTitle() );
        mMenuItemPriceTextView.setText( String.valueOf( menuItem.getPrice() ) );
    }

    public interface OnMenuItemSelectedListener
    {
        public void onMenuItemSelected(MenuItem menuItem);
    }
}
