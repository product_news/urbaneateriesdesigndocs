package com.android.learn.urbaneateries.enumdef;

/**
 * Created by jame_h on 10/5/2016.
 */

public enum EnumLoginMethod
{
    FACEBOOK,
    TWITTER,
    GOOGLE,
    OTHER_EMAIL,
}
