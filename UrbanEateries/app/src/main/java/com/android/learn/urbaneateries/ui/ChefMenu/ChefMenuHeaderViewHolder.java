package com.android.learn.urbaneateries.ui.ChefMenu;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.learn.urbaneateries.R;

/**
 * Created by Ralf on 10/8/2016.
 */

public class ChefMenuHeaderViewHolder extends RecyclerView.ViewHolder
{
    private TextView mMenuHeaderTextView;

    public ChefMenuHeaderViewHolder( View menuView )
    {
        super( menuView );
        mMenuHeaderTextView = (TextView) menuView.findViewById( R.id.fragment_chef_menu_header_text_view );
    }

    public void setMenuHeaderLabel( String headerLabel )
    {
        mMenuHeaderTextView.setText( headerLabel );
    }

}
