package com.android.learn.urbaneateries.model;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jame_h on 10/5/2016.
 */

public class MenuItem implements Parcelable
{
    public static final Creator<MenuItem> CREATOR = new Creator<MenuItem>()
    {
        @Override
        public MenuItem createFromParcel( Parcel in )
        {
            return new MenuItem( in );
        }

        @Override
        public MenuItem[] newArray( int size )
        {
            return new MenuItem[size];
        }
    };
    String mID;
    String mMenuItemImageUrl;
    String mTitle;
    String mDescription;
    double mPrice;

    public MenuItem( JSONObject jsonObject )
    {
        try
        {
            mID = jsonObject.getString( "objectId" );
            mTitle = jsonObject.getString( "Title" );
            mDescription = jsonObject.getString( "Description" );
            mPrice = jsonObject.getDouble( "Price" );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }
    }

    protected MenuItem( Parcel in )
    {
        mID = in.readString();
        mMenuItemImageUrl = in.readString();
        mTitle = in.readString();
        mDescription = in.readString();
        mPrice = in.readDouble();
    }

    public String getID()
    {
        return mID;
    }

    public void setID( String ID )
    {
        mID = ID;
    }

    public String getTitle()
    {
        return mTitle;
    }

    public void setTitle( String title )
    {
        mTitle = title;
    }

    public String getDescription()
    {
        return mDescription;
    }

    public void setDescription( String description )
    {
        mDescription = description;
    }

    public double getPrice()
    {
        return mPrice;
    }

    public void setPrice( double price )
    {
        mPrice = price;
    }

    public String getMenuItemImageUrl()
    {
        return mMenuItemImageUrl;
    }

    public void setMenuItemImageUrl( String menuItemImageUrl )
    {
        mMenuItemImageUrl = menuItemImageUrl;
    }

    public JSONObject toJason()
    {
        JSONObject jsonObject = new JSONObject();

        try
        {
            jsonObject.put( "___class", "MenuItem" );

            jsonObject.put( "Title", this.mTitle );
            jsonObject.put( "Description", this.mDescription );
            jsonObject.put( "Price", this.mPrice );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( mID );
        dest.writeString( mMenuItemImageUrl );
        dest.writeString( mTitle );
        dest.writeString( mDescription );
        dest.writeDouble( mPrice );
    }
}
