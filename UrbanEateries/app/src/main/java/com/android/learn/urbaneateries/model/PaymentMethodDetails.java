package com.android.learn.urbaneateries.model;

import com.android.learn.urbaneateries.enumdef.EnumPaymentMethod;

/**
 * Created by jame_h on 10/5/2016.
 */
public class PaymentMethodDetails
{
    EnumPaymentMethod mPaymentDetailList;
    int mCardNumber;
    int mExpirationMonth;
    int mExpirationYear;
    int mSecurityCode;
    int mBillingZipCode;
}
