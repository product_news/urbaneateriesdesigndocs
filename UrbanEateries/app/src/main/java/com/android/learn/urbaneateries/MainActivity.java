package com.android.learn.urbaneateries;


import android.app.Activity;
import android.content.Intent;

import android.support.annotation.NonNull;

import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;

import com.android.learn.urbaneateries.ui.ChefList.ChefListFragment;


public class MainActivity extends AppCompatActivity{
    static private final String TAG = "MainActivity";
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private Toolbar mToolBar;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        mDrawerLayout = (DrawerLayout) findViewById( R.id.activity_main_drawer_layout );
        mNavigationView = (NavigationView) findViewById( R.id.activity_main_navigation_view );
        mToolBar = (Toolbar) findViewById( R.id.activity_main_toolbar );

        Init();

        setSupportActionBar( mToolBar );
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle( this, mDrawerLayout, mToolBar, R.string.open_drawer, R.string.close_drawer );
        mDrawerLayout.addDrawerListener( drawerToggle );
        drawerToggle.syncState();

        if( savedInstanceState == null )
        {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add( R.id.activity_main_vg_container, new ChefListFragment() )
                    .commit();
        }
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        super.onActivityResult( requestCode, resultCode, data );

        if( requestCode == RequestCode.SIGN_UP_REQUEST.toInt() ||
                requestCode == RequestCode.LOGIN_REQUEST.toInt() )
        {
            if( resultCode == Activity.RESULT_OK )
            {
                Menu navMenu = mNavigationView.getMenu();
                navMenu.clear();

                mNavigationView.inflateMenu( R.menu.activity_main_nav_after_login_menu );
            }
        }
    }


    public void Init()
    {
        // Prepare menu for Navigation View
        mNavigationView.inflateMenu( R.menu.activity_main_nav_before_login_menu );

        mNavigationView.setNavigationItemSelectedListener( new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected( @NonNull MenuItem item )
            {
                int menuItemId;
                Intent intent;
                Menu navMenu;
                Fragment displayFragment = null;

                menuItemId = item.getItemId();

                switch( menuItemId )
                {
                    case R.id.sign_up:
                        intent = new Intent( getApplicationContext(), ActivitySignUpLogin.class );
                        intent.putExtra( "SignUp", true );

                        startActivityForResult( intent, RequestCode.SIGN_UP_REQUEST.toInt() );
                        break;

                    case R.id.login:
                        intent = new Intent( getApplicationContext(), ActivitySignUpLogin.class );
                        intent.putExtra( "Login", true );

                        startActivityForResult( intent, RequestCode.LOGIN_REQUEST.toInt() );
                        break;

                    case R.id.after_login_my_profile:
                        break;

                    case R.id.after_login_active_order:
                        break;

                    case R.id.after_login_past_order:
                        break;

                    case R.id.after_login_setting:
                        break;

                    case R.id.after_login_contact_us:
                        break;

                    case R.id.after_login_logout:
                        navMenu = mNavigationView.getMenu();
                        navMenu.clear();

                        mNavigationView.inflateMenu( R.menu.activity_main_nav_before_login_menu );

                        break;

                }

                mDrawerLayout.closeDrawers();

                if( displayFragment != null )
                {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace( R.id.activity_main_vg_container, displayFragment )
                            .commit();
                }

                return false;
            }
        } );
    }

    private enum RequestCode
    {
        SIGN_UP_REQUEST( 0 ),
        LOGIN_REQUEST( 1 );

        private final int requestCode;

        private RequestCode( final int requestCode )
        {
            this.requestCode = requestCode;
        }

        public int toInt()
        {
            return requestCode;
        }
    }

}