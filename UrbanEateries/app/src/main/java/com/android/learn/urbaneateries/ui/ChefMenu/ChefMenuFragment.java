package com.android.learn.urbaneateries.ui.ChefMenu;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.learn.urbaneateries.R;
import com.android.learn.urbaneateries.model.MenuItem;
import com.android.learn.urbaneateries.model.UserAccount;
import com.android.learn.urbaneateries.ui.MenuItem.MenuItemDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChefMenuFragment extends Fragment implements ChefMenuItemViewHolder.OnMenuItemSelectedListener
{
    private static final String TAG = "ChefMenuFragment";
    private static final String USER_ACCOUNT = "USER_ACCOUNT";
    private RecyclerView mRecyclerView;
    private UserAccount mUserAccount;

    public static ChefMenuFragment newInstance( UserAccount userAccount )
    {
        Bundle args = new Bundle();
        args.putParcelable( USER_ACCOUNT, userAccount );

        ChefMenuFragment fragment = new ChefMenuFragment();
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        // Inflate the layout for this fragment
        super.onCreateView( inflater, container, savedInstanceState );
        View rootView = inflater.inflate( R.layout.fragment_chef_menu, container, false );
        mRecyclerView = (RecyclerView) rootView.findViewById( R.id.fragment_chef_menu_recycler_view );

        if( getArguments().getParcelable( USER_ACCOUNT ) != null )
        {
            mUserAccount = getArguments().getParcelable( USER_ACCOUNT );
        }
        else
        {
            mUserAccount = new UserAccount(  );
        }

        return rootView;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
    {
        super.onViewCreated( view, savedInstanceState );

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getContext() );
        mRecyclerView.setLayoutManager( layoutManager );

        Log.d( TAG, "onViewCreated: "  + mUserAccount.getChefAccount().getMenus().toString());

        RecyclerView.Adapter adapter = new ChefMenuAdapter( mUserAccount.getChefAccount().getMenus(), this );
        mRecyclerView.setAdapter( adapter );
    }

    @Override
    public void onMenuItemSelected( MenuItem menuItem )
    {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace( R.id.activity_main_vg_container, new MenuItemDetailsFragment() )
                .addToBackStack( TAG )
                .commit();
    }
}
