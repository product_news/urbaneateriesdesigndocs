package com.android.learn.urbaneateries.model;


import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jame_h on 10/5/2016.
 */

public class Menu implements Parcelable
{
    public static final Creator<Menu> CREATOR = new Creator<Menu>()
    {
        @Override
        public Menu createFromParcel( Parcel in )
        {
            return new Menu( in );
        }

        @Override
        public Menu[] newArray( int size )
        {
            return new Menu[size];
        }
    };
    String mID;
    String mLabel;
    List<MenuItem> mMenuItems;

    public Menu( JSONObject jsonObject )
    {
        mMenuItems = new ArrayList<>();

        try
        {
            mID = jsonObject.getString( "objectId" );
            mLabel = jsonObject.getString( "Label" );

            JSONArray jsonArray = jsonObject.getJSONArray( "MenuItem" );

            for( int i = 0; i < jsonArray.length(); i++ )
            {
                mMenuItems.add( new MenuItem( (JSONObject) jsonArray.get( i ) ) );
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }
    }

    protected Menu( Parcel in )
    {
        mID = in.readString();
        mLabel = in.readString();
        mMenuItems = in.createTypedArrayList( MenuItem.CREATOR );
    }

    public List<MenuItem> getMenuItems()
    {
        return mMenuItems;
    }

    public void setMenuItems( List<MenuItem> menuItems )
    {
        mMenuItems = menuItems;
    }

    public String getLabel()
    {
        return mLabel;
    }

    public void setLabel( String label )
    {
        mLabel = label;
    }

    public String getID()
    {
        return mID;
    }

    public void setID( String ID )
    {
        mID = ID;
    }

    public JSONObject toJson()
    {
        List<JSONObject> jsonObjects = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();

        try
        {
            jsonObject.put( "___class", "Menu" );
            jsonObject.put( "Label", this.mLabel );

            for( MenuItem menuItem : mMenuItems )
            {
                jsonObjects.add( menuItem.toJason() );
            }

            jsonObject.put( "MenuItem", new JSONArray( jsonObjects ) );

        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override public int describeContents()
    {
        return 0;
    }

    @Override public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( mID );
        dest.writeString( mLabel );
        dest.writeTypedList( mMenuItems );
    }
}


