package com.android.learn.urbaneateries;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.android.learn.urbaneateries.ui.FragmentLogin;
import com.android.learn.urbaneateries.ui.FragmentSignUp;

/**
 * Created by jamehii on 10/7/2016.
 */
public class ActivitySignUpLogin extends AppCompatActivity
{
    private Toolbar mToolbar;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_signup_login );

        Intent intent = getIntent();

        boolean login = intent.getBooleanExtra( "Login", false );
        boolean signUp = intent.getBooleanExtra( "SignUp", false );

        Fragment displayFragment = null;

        if( signUp )
        {
            displayFragment = new FragmentSignUp();
        }

        if( login )
        {
            displayFragment = new FragmentLogin();
        }

        getSupportFragmentManager()
                .beginTransaction()
                .add( R.id.activity_signup_login_container, displayFragment )
                .commit();
    }
}
