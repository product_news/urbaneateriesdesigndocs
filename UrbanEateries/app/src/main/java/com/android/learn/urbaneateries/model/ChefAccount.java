package com.android.learn.urbaneateries.model;


import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jame_h on 10/5/2016.
 */

public class ChefAccount implements Parcelable
{
    private static final String TAG = "ChefAccount";
    List<Menu> mMenus = new ArrayList<>();
    List<Review> mReviews = new ArrayList<>();

    public ChefAccount()
    {

    }


    public ChefAccount( JSONObject jsonObject )
    {
        try
        {
            JSONArray jsonArray;

            jsonArray = jsonObject.getJSONArray( "Menu" );

            for( int i = 0; i < jsonArray.length(); i++ )
            {
                mMenus.add( new Menu( (JSONObject) jsonArray.get( i ) ) );
            }

            jsonArray = jsonObject.getJSONArray( "Review" );

            for( int i = 0; i < jsonArray.length(); i++ )
            {
                mReviews.add( new Review( (JSONObject) jsonArray.get( i ) ) );
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

    }

    protected ChefAccount( Parcel in )
    {
        mMenus = in.createTypedArrayList( Menu.CREATOR );
        mReviews = in.createTypedArrayList( Review.CREATOR );
    }

    public static final Creator<ChefAccount> CREATOR = new Creator<ChefAccount>()
    {
        @Override
        public ChefAccount createFromParcel( Parcel in )
        {
            return new ChefAccount( in );
        }

        @Override
        public ChefAccount[] newArray( int size )
        {
            return new ChefAccount[size];
        }
    };

    public List<Menu> getMenus()
    {
        return mMenus;
    }

    public void setMenus( List<Menu> menus )
    {
        mMenus = menus;
    }

    public List<Review> getReviews()
    {
        return mReviews;
    }

    public void setReviews( List<Review> reviews )
    {
        mReviews = reviews;
    }

    public JSONObject toJson()
    {
        List<JSONObject> jsonObjects = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();

        try
        {
            for( Menu menu : mMenus )
            {
                jsonObjects.add( menu.toJson() );
            }

            jsonObject.put("Menu", new JSONArray( jsonObjects ));

            jsonObjects.clear();

            for( Review review : mReviews )
            {
                jsonObjects.add( review.toJson() );
            }

            jsonObject.put("Review", new JSONArray( jsonObjects ));
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override public int describeContents()
    {
        return 0;
    }

    @Override public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeTypedList( mMenus );
        dest.writeTypedList( mReviews );
    }
}
