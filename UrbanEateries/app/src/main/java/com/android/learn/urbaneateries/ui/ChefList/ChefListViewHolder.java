package com.android.learn.urbaneateries.ui.ChefList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.learn.urbaneateries.R;
import com.android.learn.urbaneateries.model.ChefAccount;
import com.android.learn.urbaneateries.model.UserAccount;

/**
 * Created by Jame on 10/21/2016.
 */
public class ChefListViewHolder extends RecyclerView.ViewHolder
{
    private int mPosition;
    private TextView mTextViewChefNickName;
    private TextView mTextViewChefEmail;
    private OnChefSelectedListener mHandler;
    private UserAccount mUserAccount;

    public ChefListViewHolder( View viewHolder, OnChefSelectedListener handler )
    {
        super( viewHolder );
        mHandler = handler;
        mTextViewChefNickName = (TextView) viewHolder.findViewById( R.id.chef_nick_name_text_view );
        mTextViewChefEmail = (TextView) viewHolder.findViewById( R.id.chef_email_text_view );

        viewHolder.setOnClickListener( new View.OnClickListener()
        {
            @Override public void onClick( View v )
            {
                mHandler.onChefSelected( mUserAccount );
            }
        } );
    }

    public void saveChefAccountToViewHolder( UserAccount userAccount, int position )
    {
        mUserAccount = userAccount;
        mPosition = position;
        mTextViewChefNickName.setText( userAccount.getNickName() );
        mTextViewChefEmail.setText( userAccount.getEmail() );
    }

    public interface OnChefSelectedListener
    {
        void onChefSelected( UserAccount chefAccount );
    }
}
