package com.android.learn.urbaneateries.ui.MenuItem;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.learn.urbaneateries.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuItemDetailsFragment extends Fragment
{

    ImageView mImageView;
    TextView mTextViewDetails;

    public MenuItemDetailsFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        // Inflate the layout for this fragment
        View rootView = (View) inflater.inflate( R.layout.fragment_menu_item_details, container, false );

        mImageView = (ImageView) rootView.findViewById( R.id.fragment_menu_item_image );
        mTextViewDetails = (TextView) rootView.findViewById( R.id.fragment_menu_item_title );

        return rootView;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
    {
        super.onViewCreated( view, savedInstanceState );
    }
}
