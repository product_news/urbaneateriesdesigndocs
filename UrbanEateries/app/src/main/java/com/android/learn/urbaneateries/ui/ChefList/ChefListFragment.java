package com.android.learn.urbaneateries.ui.ChefList;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.learn.urbaneateries.HttpApiClient;
import com.android.learn.urbaneateries.R;
import com.android.learn.urbaneateries.model.ChefAccount;
import com.android.learn.urbaneateries.model.UserAccount;
import com.android.learn.urbaneateries.ui.ChefMenu.ChefMenuFragment;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChefListFragment extends Fragment
        implements ChefListViewHolder.OnChefSelectedListener,
                   HttpApiClient.OnUsersAtOffsetListener
{
    private static final int LOAD_THRESHOLD = 5;
    private static final String TAG = "ChefListFragment";
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefresh;
    private List<UserAccount> mChefAccounts = new ArrayList<>();
    private ChefListAdapter mChefListAdapter;
    private HttpApiClient mHttpApiClient;
    private boolean mLoadInProgress;
    private int mPreviousItemCount;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState )
    {
        super.onCreateView( inflater, container, savedInstanceState );

        if( mHttpApiClient == null )
        {
            mHttpApiClient = HttpApiClient.getSharedIntance( getActivity().getApplicationContext() );
        }

        View rootView = (View) inflater.inflate( R.layout.fragment_chef_list, container, false );

        mRecyclerView = (RecyclerView) rootView.findViewById( R.id.fragment_chef_list_recycler_view );
        mSwipeRefresh = (SwipeRefreshLayout) rootView.findViewById( R.id.fragment_chef_list_swipe_refresh );

        return rootView;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
    {
        super.onViewCreated( view, savedInstanceState );

        // RecyclerView needs layout manager to determine item position
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getActivity() );
        mRecyclerView.setLayoutManager( layoutManager );

        mChefListAdapter = new ChefListAdapter( this );
        mRecyclerView.setAdapter( mChefListAdapter );

        mSwipeRefresh.setRefreshing( true );
        mHttpApiClient.getUsersAtOffset( 0, this );

        setUpOnRefreshListener();

        mLoadInProgress = false;

        mRecyclerView.addOnScrollListener( new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled( RecyclerView recyclerView, int dx, int dy )
            {
                super.onScrolled( recyclerView, dx, dy );

                int firstVisibleItemPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                int totalItemCount = mRecyclerView.getLayoutManager().getItemCount();
                int visibleItemCount = mRecyclerView.getChildCount();

                if( mPreviousItemCount != totalItemCount && mLoadInProgress )
                {
                    mLoadInProgress = false;
                    mPreviousItemCount = totalItemCount;
                }

                if( dy > 0 && !mLoadInProgress )
                {

                    if( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount )
                    {
                        mPreviousItemCount = (visibleItemCount + firstVisibleItemPosition);
                        mLoadInProgress = true;
                        mSwipeRefresh.setRefreshing( true );

                        mHttpApiClient.getUsersAtOffset( HttpApiClient.getOffset(), ChefListFragment.this );
                        Log.d( TAG, "onScrollChange: " + "LOAD MORE DATA" );
                    }
                }
            }
        } );
    }

    public void setUpOnRefreshListener()
    {
        mSwipeRefresh.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                mChefAccounts.clear();

                mHttpApiClient.getUsersAtOffset( 0, new HttpApiClient.OnUsersAtOffsetListener()
                {
                    @Override
                    public void onUsersAtOffsetComplete( JSONObject response, VolleyError error )
                    {
                        if( response != null )
                        {
                            try
                            {
                                JSONArray jsonArray = response.getJSONArray( "data" );

                                for( int i = 0; i < jsonArray.length(); i++ )
                                {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get( i );

                                    mChefAccounts.add( new UserAccount( jsonObject ) );
                                }

                                mChefListAdapter.setChefAccounts( mChefAccounts );
                                mChefListAdapter.notifyDataSetChanged();
                            }
                            catch( JSONException e )
                            {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            Log.d( TAG, "onUserAccountLoadedComplete: " + error.toString() );
                        }

                        mSwipeRefresh.setRefreshing( false );
                    }
                } );
            }
        } );
    }

    @Override
    public void onChefSelected( UserAccount chefAccount )
    {
        Log.d( TAG, "onChefSelected: " );

        ChefMenuFragment chefMenuFragment = ChefMenuFragment.newInstance( chefAccount );

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace( R.id.activity_main_vg_container, chefMenuFragment )
                .addToBackStack( TAG )
                .commit();

    }

    @Override
    public void onUsersAtOffsetComplete( JSONObject response, VolleyError error )
    {
        mSwipeRefresh.setRefreshing( false );

        if( response != null )
        {
            mChefAccounts.clear();

            try
            {
                JSONArray jsonArray = response.getJSONArray( "data" );

                if( jsonArray.length() > 0 )
                {
                    for( int i = 0; i < jsonArray.length(); i++ )
                    {
                        JSONObject jsonObject = (JSONObject) jsonArray.get( i );

                        mChefAccounts.add( new UserAccount( jsonObject ) );
                    }

                    int positionStart = mChefListAdapter.getChefAccountsSize();
                    mChefListAdapter.appendChefAccounts( mChefAccounts );
                    mChefListAdapter.notifyItemRangeInserted( positionStart, mChefAccounts.size() );

                    Log.d( TAG, "onUsersAtOffsetComplete: " + mChefAccounts.size() );
                }
            }
            catch( JSONException e )
            {
                e.printStackTrace();
            }
        }
        else
        {
            Log.d( TAG, "onUsersAtOffsetComplete: " + error.toString() );
        }
    }
}
