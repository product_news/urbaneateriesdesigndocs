package com.android.learn.urbaneateries.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by jame_h on 10/5/2016.
 */
public class Review implements Parcelable
{
    public static final Creator<Review> CREATOR = new Creator<Review>()
    {
        @Override
        public Review createFromParcel( Parcel in )
        {
            return new Review( in );
        }

        @Override
        public Review[] newArray( int size )
        {
            return new Review[size];
        }
    };
    String mID;
    UserAccount mReviewer;
    String mDescription;
    int mRating;

    public Review( JSONObject jsonObject )
    {
        try
        {
            mID = jsonObject.getString( "objectID" );
            mReviewer = (UserAccount) jsonObject.get( "Reviewer" );
            mDescription = jsonObject.getString( "Description" );
            mRating = jsonObject.getInt( "Rating" );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }
    }

    protected Review( Parcel in )
    {
        mID = in.readString();
        mReviewer = in.readParcelable( UserAccount.class.getClassLoader() );
        mDescription = in.readString();
        mRating = in.readInt();
    }

    public JSONObject toJson()
    {
        JSONObject jsonObject = new JSONObject();

        try
        {
            jsonObject.put( "Reviewer", this.mReviewer.toJson() );
            jsonObject.put( "Description", this.mDescription );
            jsonObject.put( "Rating", this.mRating );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override public int describeContents()
    {
        return 0;
    }

    @Override public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( mID );
        dest.writeParcelable( mReviewer, flags );
        dest.writeString( mDescription );
        dest.writeInt( mRating );
    }
}
