package com.android.learn.urbaneateries.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jame_h on 10/5/2016.
 */
public class UserAccount extends Account implements Parcelable
{
    public static final Creator<UserAccount> CREATOR = new Creator<UserAccount>()
    {
        @Override
        public UserAccount createFromParcel( Parcel in )
        {
            return new UserAccount( in );
        }

        @Override
        public UserAccount[] newArray( int size )
        {
            return new UserAccount[size];
        }
    };
    //List<PaymentMethodDetails> mPaymentMethodList;
    //ShoppingCart mShoppingCart;
    //List<Order> mOrders;
    ChefAccount mChefAccount = new ChefAccount();

    public UserAccount()
    {

    }

    public UserAccount( JSONObject jsonObject )
    {
        super.toModel( jsonObject );

        try
        {
            JSONArray jsonArray = (JSONArray) jsonObject.getJSONArray( "ChefAccount" );

            if( jsonArray.length() > 0 )
            {
                mChefAccount = new ChefAccount( jsonArray.getJSONObject( 0 ) );
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }
    }

    protected UserAccount( Parcel in )
    {
        mChefAccount = in.readParcelable( ChefAccount.class.getClassLoader() );
    }

    public ChefAccount getChefAccount()
    {
        return mChefAccount;
    }

    public void setChefAccount( ChefAccount chefAccount )
    {
        mChefAccount = chefAccount;
    }

    public JSONObject toJson()
    {
        List<JSONObject> jsonObjects = new ArrayList<>();
        JSONObject jsonObject = super.toJson();

        try
        {
            jsonObjects.add( this.toJson() );
            jsonObject.put( "ChefAccount", new JSONArray( jsonObjects ) );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override public int describeContents()
    {
        return 0;
    }

    @Override public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeParcelable( mChefAccount, flags );
    }
}
