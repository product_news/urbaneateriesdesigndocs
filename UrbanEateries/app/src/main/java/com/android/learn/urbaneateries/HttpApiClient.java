package com.android.learn.urbaneateries;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by jame_h on 10/10/2016.
 */

public class HttpApiClient
{
    private static final String TAG = "HttpApiClient";
    private static HttpApiClient sSharedInstance;
    private static RequestQueue sRequestQueue;
    private static int sOffset;
    private static final int PAGE_SIZE = 12;


    private HttpApiClient()
    {

    }

    public static int getOffset()
    {
        return sOffset;
    }

    public static synchronized HttpApiClient getSharedIntance( Context context )
    {
        if( sSharedInstance == null )
        {
            sSharedInstance = new HttpApiClient();
            sRequestQueue = Volley.newRequestQueue( context );
            sOffset = 0;
        }

        return sSharedInstance;
    }

    public void getUsersAtOffset( int offset, final OnUsersAtOffsetListener handler )
    {
        String url = "https://api.backendless.com/v1/data/Users?pageSize=" + PAGE_SIZE;

        if( offset == 0 )
        {
            sOffset = 0;
        }
        Log.d( TAG, "getUsersAtOffset: " + sOffset );

        if( offset > 0 )
        {
            url += "&offset=" + offset;
        }

		url += "&relationsDepth=3";
        sOffset += PAGE_SIZE;

        JsonObjectRequest request = new JsonObjectRequestBackendless( Request.Method.GET, url, null,
                new Response.Listener< JSONObject >()
                {
                    @Override
                    public void onResponse( JSONObject response )
                    {
                        handler.onUsersAtOffsetComplete( response, null );
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse( VolleyError error )
                    {
                        handler.onUsersAtOffsetComplete( null, error );
                    }
                } );

        sRequestQueue.add( request );
    }

    public void deleteAddress( String objectId, final OnDeleteAddressListener handler )
    {
        String url = "https://api.backendless.com/v1/data/Address/" + objectId;

        JsonObjectRequest request = new JsonObjectRequestBackendless( Request.Method.DELETE, url, null,
                new Response.Listener< JSONObject >()
                {
                    @Override
                    public void onResponse( JSONObject response )
                    {
                        handler.OnDeleteAddressComplete( response, null );
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse( VolleyError error )
                    {
                        handler.OnDeleteAddressComplete( null, error );
                    }
                } );

        sRequestQueue.add( request );
    }

    public void verifyLoginUser( JSONObject jsonObject, final OnVerifyLoginListener handler )
    {
        String url = "https://api.backendless.com/v1/users/login";

        JsonObjectRequest request = new JsonObjectRequestBackendless( Request.Method.POST, url, jsonObject, new Response.Listener< JSONObject >()
        {
            @Override
            public void onResponse( JSONObject response )
            {
                handler.onVerifyLoginComplete( response, null );
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse( VolleyError error )
            {
                handler.onVerifyLoginComplete( null, error );
            }
        } );

        sRequestQueue.add( request );
    }

    public void verifySignUpUser( JSONObject jsonObject, final OnVerifySignUpListener handler )
    {
        String url = "https://api.backendless.com/v1/users/register";

        JsonObjectRequest request = new JsonObjectRequestBackendless( Request.Method.POST, url, jsonObject, new Response.Listener< JSONObject >()
        {
            @Override
            public void onResponse( JSONObject response )
            {
                handler.onVerifySignUpComplete( response, null );
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse( VolleyError error )
            {
                handler.onVerifySignUpComplete( null, error );
            }
        } );

        sRequestQueue.add( request );
    }

    public void resetPassword( String email, final OnResetPasswordListener handler )
    {
        String url = "https://api.backendless.com/v1/users/restorepassword/" + email;

        final JsonObjectRequest request = new JsonObjectRequestBackendless( Request.Method.GET, url, null, new Response.Listener< JSONObject >()
        {
            @Override
            public void onResponse( JSONObject response )
            {
                handler.onResetPasswordComplete( response, null );
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse( VolleyError error )
            {
                handler.onResetPasswordComplete( null, error );
            }
        } );

        sRequestQueue.add(request);
    }

    public interface OnDeleteAddressListener
    {
        public void OnDeleteAddressComplete( JSONObject response, VolleyError error );
    }

    public interface OnVerifyLoginListener
    {
        public void onVerifyLoginComplete( JSONObject response, VolleyError error );
    }

    public interface OnVerifySignUpListener
    {
        public void onVerifySignUpComplete( JSONObject response, VolleyError error );
    }

    public interface OnResetPasswordListener
    {
        public void onResetPasswordComplete( JSONObject response, VolleyError error );
    }

    public interface OnUsersAtOffsetListener
    {
        public void onUsersAtOffsetComplete( JSONObject response, VolleyError error );
    }
}
