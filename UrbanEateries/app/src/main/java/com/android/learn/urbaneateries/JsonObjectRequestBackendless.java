package com.android.learn.urbaneateries;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jame_h on 10/10/2016.
 */

public class JsonObjectRequestBackendless extends JsonObjectRequest
{
    public JsonObjectRequestBackendless( int method, String url, JSONObject jsonRequest, Response.Listener< JSONObject > listener, Response.ErrorListener errorListener )
    {
        super( method, url, jsonRequest, listener, errorListener );
    }

    public JsonObjectRequestBackendless( String url, JSONObject jsonRequest, Response.Listener< JSONObject > listener, Response.ErrorListener errorListener )
    {
        super( url, jsonRequest, listener, errorListener );
    }

    @Override
    public Map< String, String > getHeaders() throws AuthFailureError
    {
        Map< String, String > headers = new HashMap<>( super.getHeaders() );

        headers.put( "application-id", "CF9B2C8F-C671-4DC9-FF15-208F2A6B9000" );
        headers.put( "secret-key", "3CC35A8C-5E8F-822F-FFBD-899271B22C00" );
        headers.put( "Content-Type", "application/json" );
        headers.put( "application-type", "REST" );

        return headers;
    }
}
