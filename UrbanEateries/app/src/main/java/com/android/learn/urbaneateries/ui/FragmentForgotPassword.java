package com.android.learn.urbaneateries.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.learn.urbaneateries.HttpApiClient;
import com.android.learn.urbaneateries.R;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jame_h on 10/13/2016.
 */

public class FragmentForgotPassword extends Fragment implements HttpApiClient.OnResetPasswordListener
{
    private static final String TAG = "FragmentForgotPassword";

    private EditText mEditTextViewEmail;
    private Button mButtonSubmit;
    private TextView mTextViewMessage;
    private Button mButtonOK;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState )
    {
        super.onCreateView( inflater, container, savedInstanceState );

        View rootView = (View) inflater.inflate( R.layout.fragment_forgot_password, container, false );
        mEditTextViewEmail = (EditText) rootView.findViewById( R.id.fragment_forgot_password_email );
        mButtonSubmit = (Button) rootView.findViewById( R.id.fragment_forgot_password_button_submit );
        mTextViewMessage = (TextView) rootView.findViewById( R.id.fragment_forgot_password_text_view_message );
        mButtonOK = (Button) rootView.findViewById( R.id.fragment_forgot_password_button_OK );

        return rootView;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
    {
        super.onViewCreated( view, savedInstanceState );

        mButtonSubmit.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mTextViewMessage.setVisibility( View.INVISIBLE );
                HttpApiClient httpApiClient = HttpApiClient.getSharedIntance( getActivity().getApplicationContext() );
                httpApiClient.resetPassword( mEditTextViewEmail.getText().toString(), FragmentForgotPassword.this );
            }
        } );

        mButtonOK.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                getActivity().finish();
            }
        } );
    }

    @Override
    public void onResetPasswordComplete( JSONObject response, VolleyError error )
    {
        String successfulMessage;

        successfulMessage = error.getMessage();

        if( successfulMessage == null )
        {
            mEditTextViewEmail.setVisibility( View.VISIBLE );
            mButtonSubmit.setVisibility( View.VISIBLE );

            mTextViewMessage.setText( "User does not exist in our database" );
            mTextViewMessage.setTextColor( Color.RED );
            mTextViewMessage.setVisibility( View.VISIBLE );
        }
        else
        {
            mEditTextViewEmail.setVisibility( View.GONE );
            mButtonSubmit.setVisibility( View.GONE );

            mTextViewMessage.setText( "Please check your mailbox " + mEditTextViewEmail.getText() + " for temporary reset password" );
            mTextViewMessage.setTextColor( Color.BLACK );
            mTextViewMessage.setVisibility( View.VISIBLE );
            mButtonOK.setVisibility( View.VISIBLE );
        }
    }
}
