package com.android.learn.urbaneateries.ui.ChefList;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.learn.urbaneateries.R;
import com.android.learn.urbaneateries.model.ChefAccount;
import com.android.learn.urbaneateries.model.UserAccount;
import com.android.learn.urbaneateries.ui.ChefList.ChefListViewHolder.OnChefSelectedListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 10/7/2016.
 */

//TO DO: To implement the following methods with the right parameters, and algorithms.
// At the moment, the parameters/variables are hardcoded
public class ChefListAdapter extends RecyclerView.Adapter
{
    private static final String TAG = "ChefListAdapter";
    private OnChefSelectedListener mChefListViewHolderListener;
    private List<UserAccount> mChefAccounts = new ArrayList<>();

    public ChefListAdapter( OnChefSelectedListener inputLocationTextClickListener )
    {
        mChefListViewHolderListener = inputLocationTextClickListener;
    }

    public List<UserAccount> getChefAccounts()
    {
        return mChefAccounts;
    }

    public void setChefAccounts( List<UserAccount> chefAccounts )
    {
        mChefAccounts.clear();
        mChefAccounts.addAll( chefAccounts );
        Log.d( TAG, "setChefAccounts: " + mChefAccounts.size() );
    }

    public void appendChefAccounts( List<UserAccount> chefAccounts )
    {
        mChefAccounts.addAll( chefAccounts );
        Log.d( TAG, "appendChefAccounts: " + chefAccounts.size() );
    }

    public int getChefAccountsSize()
    {
        return mChefAccounts.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType )
    {
        // Inflate Item View
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
        View itemView = inflater.inflate( R.layout.chef_list_view_holder, parent, false );

        // Prepare ViewHolder
        ChefListViewHolder chefListViewHolder = new ChefListViewHolder( itemView, mChefListViewHolderListener );

        return chefListViewHolder;
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, int position )
    {
        ((ChefListViewHolder) holder).saveChefAccountToViewHolder( mChefAccounts.get( position ), position );
    }

    @Override
    public int getItemCount()
    {
        return mChefAccounts.size();
    }

}
