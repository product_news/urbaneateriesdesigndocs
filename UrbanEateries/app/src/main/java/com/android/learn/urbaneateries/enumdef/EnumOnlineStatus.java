package com.android.learn.urbaneateries.enumdef;

/**
 * Created by jame_h on 10/5/2016.
 */
public enum EnumOnlineStatus
{
    AVAILABLE( 0 ),
    AWAY( 1 ),
    BUSY( 2 ),
    OFFLINE( 3 );

    int mStatus;

    EnumOnlineStatus( int status )
    {
        mStatus = status;
    }

    int toInt()
    {
        return mStatus;
    }
}
