package com.android.learn.urbaneateries.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jame_h on 10/5/2016.
 */
public class ContactNumber implements Parcelable
{
    public static final Creator<ContactNumber> CREATOR = new Creator<ContactNumber>()
    {
        @Override
        public ContactNumber createFromParcel( Parcel in )
        {
            return new ContactNumber( in );
        }

        @Override
        public ContactNumber[] newArray( int size )
        {
            return new ContactNumber[size];
        }
    };
    private static final String TABLE_NAME = "ContactNumber";
    String mID;
    String mLabel;
    int mCountryCode;
    int mAreaCode;
    int mCallNumber;

    public ContactNumber( JSONObject jsonObject )
    {
        try
        {
            mID = (String) jsonObject.get( "objectId" );
            mCallNumber = (int) jsonObject.get( "CallNumber" );
            mLabel = (String) jsonObject.get( "Label" );
            mCountryCode = (int) jsonObject.get( "CountryCode" );
            mAreaCode = (int) jsonObject.get( "AreaCode" );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }
    }

    protected ContactNumber( Parcel in )
    {
        mID = in.readString();
        mLabel = in.readString();
        mCountryCode = in.readInt();
        mAreaCode = in.readInt();
        mCallNumber = in.readInt();
    }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( mID );
        dest.writeString( mLabel );
        dest.writeInt( mCountryCode );
        dest.writeInt( mAreaCode );
        dest.writeInt( mCallNumber );
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public JSONObject toJason()
    {
        JSONObject obj = new JSONObject();

        try
        {
            obj.put( "___class", TABLE_NAME );

            obj.put( "Label", this.mLabel );
            obj.put( "CountryCode", this.mCountryCode );
            obj.put( "AreaCode", this.mAreaCode );
            obj.put( "CallNumber", this.mCallNumber );
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return obj;
    }
}
