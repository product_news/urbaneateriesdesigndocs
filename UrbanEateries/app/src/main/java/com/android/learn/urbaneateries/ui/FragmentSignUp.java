package com.android.learn.urbaneateries.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.learn.urbaneateries.HttpApiClient;
import com.android.learn.urbaneateries.R;
import com.android.learn.urbaneateries.model.LoginInfo;
import com.android.learn.urbaneateries.model.SignUpInfo;
import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by jame_h on 10/6/2016.
 */
public class FragmentSignUp extends android.support.v4.app.Fragment implements HttpApiClient.OnVerifySignUpListener
{
    private static final String TAG = "FragmentSignUp";

    EditText mEditTextEmail;
    EditText mEditTextPassword;

    Button mButtonSignUp;
    Button mButtonCancel;
    private TextView mTextViewSignUpFailed;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState )
    {
        super.onCreateView( inflater, container, savedInstanceState );

        View rootView = (View) inflater.inflate( R.layout.fragment_sign_up, container, false );

        mButtonSignUp = (Button) rootView.findViewById( R.id.fragment_signup_button_sign_up );
        mButtonCancel = (Button) rootView.findViewById( R.id.fragment_signup_button_cancel );
        mEditTextEmail = (EditText) rootView.findViewById( R.id.fragment_signup_edit_text_signup_email_add );
        mEditTextPassword = (EditText) rootView.findViewById( R.id.fragment_signup_edit_text_signup_password );
        mTextViewSignUpFailed = (TextView) rootView.findViewById( R.id.fragment_signup_text_view_signup_failed );

        return rootView;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
    {
        super.onViewCreated( view, savedInstanceState );


        mButtonSignUp.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mTextViewSignUpFailed.setVisibility( View.INVISIBLE );

                HttpApiClient httpApiClient = HttpApiClient.getSharedIntance( getActivity().getApplicationContext() );
                SignUpInfo signUpInfo = new SignUpInfo( mEditTextEmail.getText().toString(), mEditTextPassword.getText().toString() );
                JSONObject jsonObject = signUpInfo.toJson();

                httpApiClient.verifySignUpUser( jsonObject, FragmentSignUp.this );
            }
        } );

        mButtonCancel.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                getActivity().setResult( Activity.RESULT_CANCELED );
                getActivity().finish();
            }
        } );
    }

    @Override
    public void onVerifySignUpComplete( JSONObject response, VolleyError error )
    {
        if( response != null )
        {
            getActivity().setResult( Activity.RESULT_OK );
            getActivity().finish();
            Log.d( TAG, "onVerifySignUpComplete: Success" );
        }
        else
        {
            mTextViewSignUpFailed.setVisibility( View.VISIBLE );
            mEditTextPassword.setText( "" );

            Log.d( TAG, "onVerifySignUpComplete: " + error.toString() );
        }
    }
}
