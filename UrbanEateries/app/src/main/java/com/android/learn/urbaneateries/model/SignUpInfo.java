package com.android.learn.urbaneateries.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jame_h on 10/5/2016.
 */

public class SignUpInfo
{
    String mEmailAddress;
    String mPassword;

    public SignUpInfo( String emailAddress, String password )
    {
        mEmailAddress = emailAddress;
        mPassword = password;
    }

    public String getEmailAddress()
    {
        return mEmailAddress;
    }

    public void setEmailAddress( String emailAddress )
    {
        mEmailAddress = emailAddress;
    }

    public String getPassword()
    {
        return mPassword;
    }

    public void setPassword( String password )
    {
        mPassword = password;
    }

    public JSONObject toJson()
    {
        JSONObject jsonObject = new JSONObject();

        try
        {
            jsonObject.put( "email", this.mEmailAddress );
            jsonObject.put( "password", this.mPassword );
        } catch( JSONException e )
        {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
