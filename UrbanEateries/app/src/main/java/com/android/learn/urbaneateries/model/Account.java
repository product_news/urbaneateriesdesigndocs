package com.android.learn.urbaneateries.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jame_h on 10/5/2016.
 */

public class Account implements Parcelable
{
    public static final Creator<Account> CREATOR = new Creator<Account>()
    {
        @Override
        public Account createFromParcel( Parcel in )
        {
            return new Account( in );
        }

        @Override
        public Account[] newArray( int size )
        {
            return new Account[size];
        }
    };
    protected static final String TABLE_NAME = "Users";
    String mID;
    String mProfileImageUrl;
    String mFirstName;
    String mMiddleName;
    String mLastName;
    String mNickName;
    double mLatitude;
    double mLongitude;
    String mEmail;
    boolean mChefEnabled;
    List<ContactNumber> mContactNumbers = new ArrayList<>();
    List<Address> mAddresses = new ArrayList<>();

    public Account()
    {
    }

    protected Account( Parcel in )
    {
        mID = in.readString();
        mProfileImageUrl = in.readString();
        mFirstName = in.readString();
        mMiddleName = in.readString();
        mLastName = in.readString();
        mNickName = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mEmail = in.readString();
        mChefEnabled = in.readByte() != 0;
        mContactNumbers = in.createTypedArrayList( ContactNumber.CREATOR );
        mAddresses = in.createTypedArrayList( Address.CREATOR );
    }

    public String getProfileImageUrl()
    {
        return mProfileImageUrl;
    }

    public void setProfileImageUrl( String profileImageUrl )
    {
        mProfileImageUrl = profileImageUrl;
    }

    public double getLongitude()
    {
        return mLongitude;
    }

    public void setLongitude( double longitude )
    {
        mLongitude = longitude;
    }

    public double getLatitude()
    {
        return mLatitude;
    }

    public void setLatitude( double latitude )
    {
        mLatitude = latitude;
    }

    public String getID()
    {
        return mID;
    }

    public void setID( String ID )
    {
        mID = ID;
    }

    public String getFirstName()
    {
        return mFirstName;
    }

    public void setFirstName( String firstName )
    {
        mFirstName = firstName;
    }

    public String getMiddleName()
    {
        return mMiddleName;
    }

    public void setMiddleName( String middleName )
    {
        mMiddleName = middleName;
    }

    public String getLastName()
    {
        return mLastName;
    }

    public void setLastName( String lastName )
    {
        mLastName = lastName;
    }

    public String getNickName()
    {
        return mNickName;
    }

    public void setNickName( String nickName )
    {
        mNickName = nickName;
    }

    public String getEmail()
    {
        return mEmail;
    }

    public void setEmail( String email )
    {
        mEmail = email;
    }

    public boolean isChefEnabled()
    {
        return mChefEnabled;
    }

    public void setChefEnabled( boolean chefEnabled )
    {
        mChefEnabled = chefEnabled;
    }

    public List<Address> getAddresses()
    {
        return mAddresses;
    }

    public void setAddresses( List<Address> addresses )
    {
        mAddresses = addresses;
    }

    public List<ContactNumber> getPhoneNumbers()
    {
        return mContactNumbers;
    }

    public void setPhoneNumbers( List<ContactNumber> contactNumbers )
    {
        mContactNumbers = contactNumbers;
    }

    public void toModel( JSONObject jsonObject )
    {
        try
        {
            mID = jsonObject.getString( "objectId" );
            mFirstName = jsonObject.getString( "FirstName" );
            mMiddleName = jsonObject.getString( "MiddleName" );
            mLastName = jsonObject.getString( "LastName" );
            mNickName = jsonObject.getString( "NickName" );
            mEmail = jsonObject.getString( "email" );
            mChefEnabled = jsonObject.getBoolean( "ChefEnabled" );
            mLongitude = jsonObject.getDouble( "Longitude" );
            mLatitude = jsonObject.getDouble( "Latitude" );

            JSONArray jsonArray = jsonObject.getJSONArray( "Address" );

            for( int i = 0; i < jsonArray.length(); i++ )
            {
                mAddresses.add( new Address( (JSONObject) jsonArray.get( i ) ) );
            }

            jsonArray = jsonObject.getJSONArray( "ContactNumber" );

            for( int i = 0; i < jsonArray.length(); i++ )
            {
                mContactNumbers.add( new ContactNumber( (JSONObject) jsonArray.get( i ) ) );
            }

        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }
    }

    public JSONObject toJson()
    {
        List<JSONObject> jsonObjects = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();

        try
        {
            jsonObject.put( "___class", TABLE_NAME );

            jsonObject.put( "objectID", this.mID );
            jsonObject.put( "FirstName", this.mFirstName );
            jsonObject.put( "MiddleName", this.mMiddleName );
            jsonObject.put( "LastName", this.mLastName );
            jsonObject.put( "NickName", this.mNickName );
            jsonObject.put( "Email", this.mEmail );
            jsonObject.put( "Longitude", this.mLongitude );
            jsonObject.put( "Latitude", this.mLatitude );

            for( Address address : mAddresses )
            {
                jsonObjects.add( address.toJson() );
            }

            jsonObject.put( "Address", new JSONArray( jsonObjects ) );
            jsonObjects.clear();

            for( ContactNumber contactNumber : mContactNumbers )
            {
                jsonObjects.add( contactNumber.toJason() );
            }

            jsonObject.put( "ContactNumber", new JSONArray( jsonObjects ) );

        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override public int describeContents()
    {
        return 0;
    }

    @Override public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( mID );
        dest.writeString( mProfileImageUrl );
        dest.writeString( mFirstName );
        dest.writeString( mMiddleName );
        dest.writeString( mLastName );
        dest.writeString( mNickName );
        dest.writeDouble( mLatitude );
        dest.writeDouble( mLongitude );
        dest.writeString( mEmail );
        dest.writeByte( (byte) (mChefEnabled ? 1 : 0) );
        dest.writeTypedList( mContactNumbers );
        dest.writeTypedList( mAddresses );
    }
}
