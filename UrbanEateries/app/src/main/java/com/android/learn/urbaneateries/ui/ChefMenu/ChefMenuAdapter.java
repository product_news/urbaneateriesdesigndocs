package com.android.learn.urbaneateries.ui.ChefMenu;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.learn.urbaneateries.R;
import com.android.learn.urbaneateries.model.Menu;
import com.android.learn.urbaneateries.model.MenuItem;
import com.android.learn.urbaneateries.ui.ChefMenu.ChefMenuItemViewHolder.OnMenuItemSelectedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ralf on 10/8/2016.
 */

public class ChefMenuAdapter extends RecyclerView.Adapter
{
    private static final String TAG = "ChefMenuAdapter";
    private static final int MENU_HEADER = 1;
    private static final int MENU_ITEM = 2;

    List<Menu> mMenus = new ArrayList<>();
    OnMenuItemSelectedListener mMenuItemSelectedListener;

    public ChefMenuAdapter( List<Menu> menus, OnMenuItemSelectedListener menuItemSelectedListener )
    {
        if( menus != null )
        {
            mMenus.clear();
            mMenus.addAll( menus );
        }
        mMenuItemSelectedListener = menuItemSelectedListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType )
    {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
        View itemView;
        RecyclerView.ViewHolder viewHolder;

        if( viewType == MENU_HEADER )
        {
            itemView = inflater.inflate( R.layout.fragment_chef_menu_header_view_holder, parent, false );

            viewHolder = new ChefMenuHeaderViewHolder( itemView );
        }
        else
        {
            itemView = inflater.inflate( R.layout.fragment_chef_menu_item_view_holder, parent, false );
            viewHolder = new ChefMenuItemViewHolder( itemView, mMenuItemSelectedListener );
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, int position )
    {
        if( holder instanceof ChefMenuHeaderViewHolder )
        {
            Menu menu = (Menu) getItem( position );
            if( menu != null )
            {
                ((ChefMenuHeaderViewHolder) holder).setMenuHeaderLabel( menu.getLabel() );
            }
        }

        if( holder instanceof ChefMenuItemViewHolder )
        {
            MenuItem menuItem = (MenuItem) getItem( position );

            if( menuItem != null )
            {
                ((ChefMenuItemViewHolder) holder).setMenuItemDetails( menuItem );
            }
        }
    }

    @Override
    public int getItemCount()
    {
        int itemCount = 0;

        if( mMenus != null )
        {
            for( Menu menu : mMenus )
            {
                itemCount++;
                itemCount += menu.getMenuItems().size();
            }
        }

        Log.d( TAG, "getItemCount: " + itemCount );

        return itemCount;
    }

    @Override
    public int getItemViewType( int position )
    {
        super.getItemViewType( position );

        Object itemObject = getItem( position );

        if( itemObject instanceof Menu )
        {
            return MENU_HEADER;
        }
        else
        {
            return MENU_ITEM;
        }
    }

    public Object getItem( int position )
    {
        int itemPosition = 0;
        for( Menu menu : mMenus )
        {
            if( itemPosition == position )
            {
                return menu;
            }

            itemPosition++;

            for( MenuItem menuItem : menu.getMenuItems() )
            {
                if( itemPosition == position )
                {
                    return menuItem;
                }
                itemPosition++;
            }

        }

        return null;
    }
}
